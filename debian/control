Source: golang-github-apparentlymart-go-versions
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Thorsten Alteholz <debian@alteholz.de>
Build-Depends: debhelper-compat (= 12),
               dh-golang,
               golang-any,
               golang-github-davecgh-go-spew-dev,
               golang-github-go-test-deep-dev,
               golang-github-kylelemons-godebug-dev
Standards-Version: 4.5.0
Homepage: https://github.com/apparentlymart/go-versions
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-apparentlymart-go-versions
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-apparentlymart-go-versions.git
XS-Go-Import-Path: github.com/apparentlymart/go-versions
Testsuite: autopkgtest-pkg-go

Package: golang-github-apparentlymart-go-versions-dev
Architecture: all
Depends: ${misc:Depends},
         golang-github-davecgh-go-spew-dev,
         golang-github-go-test-deep-dev,
         golang-github-kylelemons-godebug-dev
Description: version-wrangling library for Go
 This package contains a library for wrangling versions, lists of versions,
 and sets of versions. Its idea of "version" is that defined by semantic
 versioning (https://semver.org/).
 .
 There are many Go libraries out there for dealing with versions in
 general and semantic versioning in particular, but many of them don't
 meet all of the following requirements that this library seeks to meet:
  - Version string and constraint string parsing with good, user-oriented
    error messages in case of syntax problems.
  - Built-in mechanisms for filtering and sorting lists of candidate
    versions based on constraints.
  - Ergonomic API for the calling application.
